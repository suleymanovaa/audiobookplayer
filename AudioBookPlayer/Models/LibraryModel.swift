//
//  LibraryModdel.swift
//  AudioBookPlayer
//
//  Created by Сулейманов Алексей on 29/05/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import Foundation

class Library {
    
    // MARK: Pablic Properties
    
    var name: String = "Test Library"
    var url: String = "https://owncloud.pstu.ru/remote.php/webdav"
    var login: String = "audiobook"
    var password: String = "audiobook"
    var path: String = "/"
    var playlists: [Playlist]? = []
    
    // MARK: Private Methods
    
    init(path: String){
        self.path = path
    }
    
}
