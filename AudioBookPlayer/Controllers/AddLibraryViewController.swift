//
//  AddLibraryViewController.swift
//  AudioBookPlayer
//
//  Created by Сулейманов Алексей on 16/05/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import UIKit
import FilesProvider

class AddLibraryViewController: UIViewController {

    // MARK: - Private Properties
    
    private enum Constants {
        static let fileManagerIdentifier = "FileManagerCell"
    }
    
    private var folderList: [FileObject]! = []
    private var webDAV: WebDAVFileProvider?
    private var webDAVPath = URL(fileURLWithPath: "/")
    private var completionHandler: (([FileObject], Error?) -> Void)!
    
    @IBOutlet private weak var connectButton: UIButton!
    @IBOutlet private weak var browserTableView: UITableView!
    @IBOutlet private weak var serverURLTextField: UITextField!
    @IBOutlet private weak var loginTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var addLibraryButton: UIButton!
    
    // MARK: - Internal Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Private Methods
    
    @IBAction private func touchConnectButton(_ sender: Any) {
        let login = loginTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        guard loginTextField.text != "" || passwordTextField.text != "" || serverURLTextField.text != "" else {
            let alertViewControler = UIAlertController(title: "Ошибка!", message: "Заполните все поля!", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alertViewControler.addAction(cancelAction)
            self.present(alertViewControler, animated: true, completion: nil)
            return
        }
        
        if let url = URL(string: serverURLTextField.text ?? "") {
            connectToServer(url: url, login: login, password: password)
        } else {
            print("url is nil")
        }
    }
    
    private func connectToServer(url: URL, login: String, password: String) {
        
        let credential = URLCredential(user: login, password: password, persistence: .permanent)
        webDAV = WebDAVFileProvider(baseURL: url, credential: credential)
        webDAV?.delegate = self as? FileProviderDelegate
        print(self.webDAVPath.absoluteString)
        
        completionHandler = { [weak self]
            contents,error in
            
            guard error == nil else {
                print (error?)
                return
            }
            
            let directories = contents.filter { $0.isDirectory }
            self?.folderList = directories
            DispatchQueue.main.async {
                self?.browserTableView.reloadData()
            }
        }
        webDAV?.contentsOfDirectory(path: self.webDAVPath.path.removingPercentEncoding ?? "/", completionHandler: completionHandler)
    }
}

// MARK: - UITableViewDataSource

extension AddLibraryViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
        guard section  != 0 else {
            return 1
        }
        
        return folderList?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:  Constants.fileManagerIdentifier, for: indexPath)
        let text: String
        
        if indexPath.section == 0 {
            text = webDAVPath.lastPathComponent
            cell.backgroundColor = .green
        }
        else {
            let file = folderList[indexPath.row]
            text = file.name
        }
        cell.textLabel?.text = text
        return cell
    }
}

// MARK: - UITableViewDelegate

extension AddLibraryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 && webDAVPath.absoluteString != "/" {
                webDAVPath.deleteLastPathComponent()
        }
        else {
             webDAVPath.appendPathComponent((tableView.cellForRow(at: indexPath)?.textLabel?.text) ?? "")
        }
        webDAV?.contentsOfDirectory(path: self.webDAVPath.path.removingPercentEncoding ?? "/", completionHandler: completionHandler)
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        print("Tap cell button!")
    }
}

