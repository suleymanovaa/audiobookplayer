//
//  PlayerViewController.swift
//  AudioBookPlayer
//
//  Created by Сулейманов Алексей on 27/05/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import UIKit
import AVFoundation
import FilesProvider

//Класс заглушка
class File {
    var name: String = "test.mp3"
    var progress: TimeInterval = TimeInterval(0)
    var path: String? = Bundle.main.path(forResource: "test", ofType: "mp3")
    
    init (name: String) {
        self.name = name
        self.path = Bundle.main.path(forResource: name, ofType: nil)
        self.progress = 0
    }
    
    init (name: String, path: String){
        self.name = name
        self.path = path
        self.progress = 0
    }
}

class Playlist {
    var name: String = "Book"
    var fileIndex: Int = 0
    var files: [File] = [File.init(name: "01 The Trail.mp3"),
                         File.init(name: "02 Geralt of Rivia.mp3"),
                         File.init(name: "03 Eredin, King of the Hunt.mp3"),
                         File.init(name: "04 Wake Up, Ciri.mp3"),
                         File.init(name: "05 Aen Seidhe.mp3")]
    init(name: String) {
        self.name = name
        self.fileIndex = 0
        self.files = [File.init(name: "01 The Trail.mp3", path: "/audiobook/01 The Trail.mp3"),
                      File.init(name: "02 Geralt of Rivia.mp3", path: "/audiobook/02 Geralt of Rivia.mp3" ),
                      File.init(name: "03 Eredin, King of the Hunt.mp3", path: "/audiobook/03 Eredin, King of the Hunt.mp3" ),
                      File.init(name: "04 Wake Up, Ciri.mp3", path: "/audiobook/04 Wake Up, Ciri.mp3" ),
                      File.init(name: "05 Aen Seidhe.mp3", path: "/audiobook/05 Aen Seidhe.mp3" )]
    }
    
    func fileProgress () -> File {
        return files[fileIndex]
    }
    
    func nextFile () -> File {
        guard fileIndex < files.count-1 else {
            return files[fileIndex]
        }
        fileIndex += 1
        return files[fileIndex]
    }
    
    func previuosFile () -> File {
        guard fileIndex != 0 else {
            return files[fileIndex]
        }
        fileIndex -= 1
        return files[fileIndex]
    }
    
}



class PlayerViewController: UIViewController {

    // MARK : Private Type
    
    private enum Constants {
        static let playlistIdentifier = "playlistCell"
        static let rewind1:Double = 10
        static let rewind2:Double = 60
    }
    
    // MARK : Private Type
    
    private enum Constants {
        static let playlistIdentifier = "playlistCell"
        static let rewind1:Double = 10
        static let rewind2:Double = 60
    }
    
    // MARK : Private Propertis
    
    @IBOutlet private weak var playButton: UIButton!
    @IBOutlet private weak var playlistTableView: UITableView!
    @IBOutlet private weak var nextButton: UIButton!
    @IBOutlet private weak var previousButton: UIButton!
    @IBOutlet private weak var rewindTenSecondAheadButton: UIButton!
    @IBOutlet private weak var fileProgressView: UIProgressView!
    @IBOutlet private weak var rewindOneMinuteAheadButton: UIButton!
    @IBOutlet private weak var rewindOneMinuteBackButton: UIButton!
    @IBOutlet private weak var rewindTenSecondBackButton: UIButton!
    @IBOutlet private weak var fileNameLabel: UILabel!
    private var library: Library? = Library(path: "/")
    private var player: AVAudioPlayer?
    private var playlist: Playlist?
    private var webDAV: WebDAVFileProvider?
    
    // MARK : Private Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        connectToServer(url: URL(string: library?.url ?? "")!, login: library?.login ?? "", password: library?.password ?? "")
        playlist = Playlist(name: "Testbook")
        playlistTableView.reloadData()
        playFile()
    }
    
    private func playFile() {
        guard let path = playlist?.fileProgress().path else {
            return
        }
        
        let completionHandler:((Data?, Error?) -> Void)! = { [weak self]
            contents,error in
            if let contents = contents {
                DispatchQueue.main.async {
                    do {
                        self?.player = try AVAudioPlayer(data: contents)
                        self?.player?.currentTime = self?.playlist?.fileProgress().progress ?? 0
                        Timer.scheduledTimer(timeInterval: 1.0,
                                             target: self!,
                                             selector: #selector(self?.updateFileProgressView),
                                             userInfo: nil,
                                             repeats: true)
                        self?.fileNameLabel.text = self?.playlist?.fileProgress().name
                        self?.player?.play()
                    } catch {
                        print(error)
                    }
                }
            }
        }
        webDAV?.contents(path: path, completionHandler: completionHandler)
    }
    
    private func connectToServer(url: URL, login: String, password: String) {
        let credential = URLCredential(user: login, password: password, persistence: .permanent)
        webDAV = WebDAVFileProvider(baseURL: url, credential: credential)
        webDAV?.delegate = self as? FileProviderDelegate
    }
    
    @objc
    private func updateFileProgressView() {
    
        guard let player = self.player else {
            return
        }
        if player.isPlaying == true {
            let progress = Float(player.currentTime/player.duration)
            fileProgressView.setProgress(progress, animated: true)
            playlist?.fileProgress().progress = player.currentTime
        }
    }
    
    
    @IBAction private func touchPlayButton(_ sender: Any) {
        if player?.isPlaying != true {
            player?.play()
            playButton?.setTitle("play", for: .normal)
        }
        else{
            playButton?.setTitle("pause", for: .normal)
            player?.pause()
        }
    }
    
    @IBAction private func touchNextButton(_ sender: Any) {
        playlist?.nextFile()
        playFile()
    }
    
    @IBAction private func touchPreviousButton(_ sender: Any) {
        playlist?.previuosFile()
        playFile()
    }
    
    @IBAction private func touchRewindOneMinuteAheadButton(_ sender: Any) {
        player?.currentTime += Constants.rewind2
        print("+1 minute")
    }
    
    @IBAction private func touchRewindTenSecondAheadButton(_ sender: Any) {
        player?.currentTime += Constants.rewind1
        print("+10 seconds")
    }
    
    @IBAction private func touchRewindTenSecondBackButton(_ sender: Any) {
        player?.currentTime -= Constants.rewind1
        print("-10 seconds")
    }
    
    @IBAction private func touchRewindOneMinuteBachButton(_ sender: Any) {
        player?.currentTime -= Constants.rewind2
        print("-1 minute")
    }
}

// MARK: PlayerViewController: AVAudioPlayerDelegate

extension PlayerViewController: AVAudioPlayerDelegate {
    
}

// MARK: PlayerViewController: UITableViewDataSource

extension PlayerViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlist?.files.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.playlistIdentifier, for: indexPath)
        cell.textLabel?.text = playlist?.files[indexPath.row].name
        return cell
    }
}

//MARK: PlayerViewController: UITableViewDelegate

extension PlayerViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        playlist?.fileIndex = indexPath.row
        playFile()
    }
}


