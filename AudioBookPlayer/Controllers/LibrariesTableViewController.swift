//
//  LibrariesTableViewController.swift
//  AudioBookPlayer
//
//  Created by Сулейманов Алексей on 29/05/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import UIKit

class LibrariesTableViewController: UITableViewController {
    
    // MARK: Private Type
    
    private enum Constants {
        static let librariesCellIdentifier = "librariesCell"
    }
    
    // MARK: Private Properties
    
    private var libraries: [Library]?

    // MARK: Private Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        libraries = [Library(path: "/"),
                     Library(path: "/audiobook/")]

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return libraries?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.librariesCellIdentifier, for: indexPath)
        cell.textLabel?.text = libraries?[indexPath.row].name
        cell.detailTextLabel?.text = libraries?[indexPath.row].path
        return cell
    }
}
